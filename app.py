from threading import Lock, Event
from flask import Flask, render_template, session, request, jsonify, url_for
from flask_socketio import SocketIO, emit, disconnect    
import time
import random
import math 
import serial
import json
import datetime;

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)
stop_event = Event()
sensor_json_s = [];
ser = None;

class Sensor: 
    def __init__(self, led_state, led_status, datetime):
        self.led_state = led_state
        self.led_status = led_status
        self.datetime = datetime

@app.route('/')
def index():
    return render_template('index.html')

@socketio.on('connect', namespace='/test')
def test_connect():
    emit('my_response', {'data': 'Connected', 'count': 0})
    
@socketio.on('connect_uno', namespace='/test')
def connect_uno():
    global ser
    ser = serial.Serial("/dev/ttyUSB0")
    ser.baudrate = 9600
    
    
@socketio.on('disconnect_uno', namespace='/test')
def disconnect_uno():
    ser.close();

@socketio.on('connect_buttonA', namespace='/test')
def connect_buttonA():
    ser.write(b'A')
        
@socketio.on('stop_sending', namespace='/test')
def stop_sending():
    ser.write(b'C')
    stop_event.set()

@socketio.on('save_file', namespace='/test')
def save_file():
    with open('output.json', 'w') as filehandle:
        json.dump(sensor_json_s, filehandle, indent=4)
        print('File saved!')

    
@socketio.on('set_threshold', namespace='/test')
def set_threshold(message):
    threshold = message['threshold']
    ser.write(f'{threshold}\n'.encode())
    stop_event.clear()  # Ensure sending is enabled

@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected')

@socketio.on('load_file', namespace='/test')
def load_file():
    with open('output.json', 'r') as openfile:
        json_object = json.load(openfile)
        emit('load_data_js', {'jsonObject': json_object})
        
@socketio.on('start_recieving', namespace='/test')
def start_recieving():
    while True:
        global ser
        if ser.in_waiting > 0:
            read_ser = ser.readline()
            decoded_ser = read_ser.decode().strip()
            
            try:
                data = json.loads(decoded_ser)
                led_status = data.get('ldrStatus')
                led_state = data.get('ledState')
                ct = datetime.datetime.now()
                ct_str = ct.strftime("%Y-%m-%d %H:%M:%S")
                emit('serial_data', {'ledStatus': led_status, 'ledState': led_state, 'datetime': ct_str})
                sensor_object = Sensor(led_state, led_status, ct_str)
                sensor_json_s.append(sensor_object.__dict__)
            except json.JSONDecodeError:
                print("Failed to decode JSON data")


if __name__ == '__main__':
    socketio.run(app, host="0.0.0.0", port=80, debug=True)

