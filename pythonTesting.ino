#include <ArduinoJson.h>

const int ledPin = D0;
const int ldrPin = A0;

char mode = 'A';  // Default mode
bool sendData = true;  // Flag to control sending data
int threshold = 300;  // Default threshold

void setup(){
  Serial.begin(9600);
  pinMode(ledPin, OUTPUT);
  pinMode(ldrPin, INPUT);
}

void loop() {
  if (Serial.available() > 0) {
    String receivedData = Serial.readStringUntil('\n');
    if (receivedData == "A" || receivedData == "B" || receivedData == "C") {
      mode = receivedData.charAt(0);
      sendData = (mode != 'C');
      
    } else {
      threshold = receivedData.toInt();
    }
    Serial.print("Mode set to: ");
    Serial.println(mode);
    Serial.print("Threshold set to: ");
    Serial.println(threshold);
  }

  if (sendData) {
    int ldrStatus = analogRead(ldrPin);
    StaticJsonDocument<200> json;

    json["ldrStatus"] = ldrStatus;
    if ((mode == 'A' && ldrStatus < threshold) || (mode == 'B' && ldrStatus < threshold)) {
      digitalWrite(ledPin, HIGH);
      json["ledState"] = 1;
    } else {
      digitalWrite(ledPin, LOW);
      json["ledState"] = 0;
    }

    Serial.println(ldrStatus);
    delay(1000);
  }
}
